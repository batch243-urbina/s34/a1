const express = require("express");
const app = express();
const port = 4000;

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

// MOCK DATABASE
let users = [
  {
    name: "ejnpu",
    password: "123",
  },
];

// GET ROUTE /home
app.get("/home", (req, res) => {
  res.send(`Welcome to the home page`);
});

// GET ROUTE /users
app.get("/users", (req, res) => {
  res.send(users);
});

// DELETE ROUTE /delete-user
app.delete("/delete-user", (request, response) => {
  let message;
  if (users.length !== 0) {
    for (let i = 0; i < users.length; i++) {
      if (request.body.name == users[i].name) {
        message = `User ${request.body.name} has been deleted`;
        users.splice(i, 1);
        response.send(message);
        console.log(message);
        break;
      } else {
        if (request.body.name == "") {
          message = `Please input name`;
          response.send(message);
        } else {
          message = `User does not exist`;
          response.send(message);
        }
      }
    }
  } else {
    console.log(request.body.name);

    response.send(`No users registered yet`);
  }
});

// LISTEN
app.listen(port, () => console.log(`Server running at port ${port}`));
